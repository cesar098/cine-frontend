import { createApp } from 'vue'
import {createRouter, createWebHashHistory} from 'vue-router'

import App from './App.vue'

import MyDashboard from './components/pages/MyDashboard'
import MyContent from './components/pages/MyContent'
import MyForm from './components/pages/MyForm'
import MyDetail from './components/pages/MyDetail'

const routes = [
    {
        path:'/inicio', 
        component: MyDashboard,
        children:[
            {path:'', component: MyContent},
            {path:'registrar', component: MyForm},
            {path:'pelicula', component: MyDetail}
        ]
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

const app = createApp(App)
app.use(router)
app.mount('#app')